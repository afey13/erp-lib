package aes256

import (
	"crypto/md5"
	"encoding/hex"
)

type HashMD5 interface {
	Encrypt(stringToEncrypt string) string
}

type HashMD5Impl struct {
}

func NewHashMD5() *HashMD5Impl {
	return &HashMD5Impl{}
}

func (x *HashMD5Impl) Encrypt(stringToEncrypt string) string {
	hash := md5.Sum([]byte(stringToEncrypt))
	return hex.EncodeToString(hash[:])
}
