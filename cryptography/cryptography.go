package cryptography

import (
	"gitlab.com/afey13/erp-lib/errors"
)

type Cryptography interface {
	Encrypt(stringToEncrypt string) (string, errors.CodedError)
	Decrypt(encryptedString string) (string, errors.CodedError)
}
