package pubsub

import (
	"context"

	"github.com/ThreeDotsLabs/watermill/message"
)

type HandlerRegistry interface {
	RegisterHandlerToRouter(r *message.Router) error
}

type HandlerMiddlewaresRegistry interface {
	GetHandlerMiddlewares() []message.HandlerMiddleware
}

type Runner struct {
	router                       *message.Router
	handlerRegistries            []HandlerRegistry
	handlerMiddlewaresRegistries []HandlerMiddlewaresRegistry
}

func NewRunner(router *message.Router) *Runner {
	return &Runner{router: router}
}

func (r *Runner) AddHandlerRegistry(registry HandlerRegistry) {
	r.handlerRegistries = append(r.handlerRegistries, registry)
}

func (r *Runner) AddHandlerMiddlewaresRegistry(registry HandlerMiddlewaresRegistry) {
	r.handlerMiddlewaresRegistries = append(r.handlerMiddlewaresRegistries, registry)
}

func (r Runner) Run(context context.Context) error {
	err := r.applyHandlerMiddlewaresToRouter()
	if err != nil {
		return err
	}
	err = r.applyHandlerToRouter()
	if err != nil {
		return err
	}
	return r.router.Run(context)
}

func (r *Runner) applyHandlerMiddlewaresToRouter() error {
	for _, reg := range r.handlerMiddlewaresRegistries {
		for _, m := range reg.GetHandlerMiddlewares() {
			r.router.AddMiddleware(m)
		}
	}
	return nil
}

func (r *Runner) applyHandlerToRouter() error {
	for _, reg := range r.handlerRegistries {
		err := reg.RegisterHandlerToRouter(r.router)
		if err != nil {
			return err
		}
	}
	return nil
}
