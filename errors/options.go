package errors

import "fmt"

//Message options setter creator for setting CodedError message
func Message(message string) OptionsSetter {
	return func(codedError *codedError) {
		codedError.FMessage = message
	}
}

//Messagef options setter creator for setting CodedError message
func Messagef(message string, args ...interface{}) OptionsSetter {
	return Message(fmt.Sprintf(message, args...))
}

//Code options setter creator for setting CodeError code
func Code(code int) OptionsSetter {
	return func(codedError *codedError) {
		codedError.FCode = code
	}
}

func PreviousError(prev error) OptionsSetter {
	return func(c *codedError) {
		c.FOriginalErr = AsCodedError(prev)
	}
}

func MessageKey(messageKey string) OptionsSetter {
	return func(c *codedError) {
		c.FMessageKey = messageKey
	}
}

func Values(params map[string]interface{}) OptionsSetter {
	return func(c *codedError) {
		c.FParams = params
	}
}
