package errors

type (
	OptionsSetter func(*codedError)

	CodedError interface {
		RootError
		Code() int
		Message() string
		MessageKey() string
		Values() map[string]interface{}
		Error() string
		PreviousError() CodedError
		CopyWith(...OptionsSetter) CodedError
		//JSON returns json representation of the error, including all nested errors and values if any.
		// DO NOT USE THIS METHOD FOR Returning error to client, use `json.Unmarshal` or `JSONEncoder.Encode` instead, this is for internal use only.
		JSON() []byte
	}

	//RootError is a root cause of error
	RootError interface {
		RootError() CodedError
	}

	//FieldError represent field error that occurred
	FieldError interface {
		Target() string
		MessageKey() string
		Message() string
		Values() map[string]interface{}
	}

	//CodedFieldError represent error that ...
	CodedFieldsError interface {
		CodedError
		FieldErrors() []FieldError
	}
)
