package header

import (
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"gitlab.com/afey13/erp-lib/config"
	"gitlab.com/afey13/erp-lib/errors"
	"gitlab.com/afey13/erp-lib/log"
	"gitlab.com/afey13/erp-lib/middleware"
	"gitlab.com/afey13/erp-lib/response"
)

type HeaderMiddlewareOpts struct {
	Config config.Config
}

type HeaderMiddlewareRegistry struct {
	options HeaderMiddlewareOpts
}

func (r HeaderMiddlewareRegistry) GetMiddlewares() chi.Middlewares {
	return chi.Middlewares{
		checkPlatform(r.options.Config),
	}
}

func NewHeaderMiddlewareRegistry(opts HeaderMiddlewareOpts) *HeaderMiddlewareRegistry {
	return &HeaderMiddlewareRegistry{
		options: opts,
	}
}

func checkPlatform(cfg config.Config) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			platforms := strings.Split(cfg.Get(config.PLATFORM), ",")
			platform := request.Header.Get(middleware.PlatformKey)
			if platform == "" {
				response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, errors.NewErrorHeaderPlatformRequired})
				return
			}
			if !isPlatform(platform, platforms) {
				response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, errors.NewErrorHeaderPlatformInvalid})
				return
			}
			request = middleware.SetWithValue(request, middleware.PlatformKey, platform)
			next.ServeHTTP(writer, request)
		})
	}
}

func isPlatform(platform string, platforms []string) bool {
	for _, val := range platforms {
		if val == platform {
			return true
		}
	}
	return false
}
