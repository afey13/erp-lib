package auth

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	jwblib "github.com/golang-jwt/jwt"
	"gitlab.com/afey13/erp-lib/config"
	"gitlab.com/afey13/erp-lib/cryptography"
	"gitlab.com/afey13/erp-lib/errors"
	"gitlab.com/afey13/erp-lib/jwt"
	"gitlab.com/afey13/erp-lib/log"
	"gitlab.com/afey13/erp-lib/middleware"
	"gitlab.com/afey13/erp-lib/response"
)

const (
	RequestIDKey   string = "requestID"
	ServiceNameKey string = "serviceName"
	DataKey        string = "data"
)

type DataDecrypt struct {
	UserID string `json:"user_id"`
}

type AuthMiddlewareOpts struct {
	Config config.Config
	Jwt    jwt.Jwt
	Crypto cryptography.Cryptography
}

type AuthMiddlewareRegistry struct {
	options AuthMiddlewareOpts
}

func (r *AuthMiddlewareRegistry) GetMiddlewares() chi.Middlewares {
	return chi.Middlewares{
		DecodeToken(r.options),
	}
}

func NewAuthMiddlewareRegistry(opts AuthMiddlewareOpts) *AuthMiddlewareRegistry {
	return &AuthMiddlewareRegistry{
		options: opts,
	}
}

func DecodeToken(opts AuthMiddlewareOpts) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			method := request.Method
			path := request.URL.Path
			paths := strings.Split(opts.Config.Get(config.MIDDLEWARE_EXCLUDE_PATH), ",")
			if !middleware.IsExcludePath(path, method, paths) {
				jwtToken := opts.Jwt.ExtractToken(request)
				token, err := jwtToken.VerifyToken()
				if err != nil {
					response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, errors.NewForbiddenAccessError()})
					return
				}
				claims, ok := token.Claims.(jwblib.MapClaims)
				if !ok {
					response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, errors.NewForbiddenAccessError()})
					return
				}
				encryptText := fmt.Sprintf("%v", claims[DataKey])
				decryptText, err := opts.Crypto.Decrypt(encryptText)
				if err != nil {
					err = errors.NewForbiddenAccessError().CopyWith(errors.Message(err.Error()))
					response.Response(log.ResponseOpts{writer, request, request.Context(), nil, 0, err})
					return
				}
				dataDecrypt := DataDecrypt{}
				_ = json.Unmarshal([]byte(decryptText), &dataDecrypt)
				request = middleware.SetWithValue(request, middleware.UserIDKey, dataDecrypt.UserID)
			}

			next.ServeHTTP(writer, request)
		})
	}
}
