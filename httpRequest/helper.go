package httpRequest

import (
	"net/http"

	"gitlab.com/afey13/erp-lib/errors"
)

type HttpRequestHelperOpts struct {
	Url    string
	Method string
	Header map[string]string
	Body   interface{}
}

type HttpRequestHelper interface {
	Request(httpValue HttpRequestHelperOpts) (*http.Response, errors.CodedError)
}
