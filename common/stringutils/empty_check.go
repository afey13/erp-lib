package stringutils

import "regexp"

func IsBlank(str string) bool {
	if str == "" {
		return true
	}
	if regexp.MustCompile(`^\s+$`).MatchString(str) {
		return true
	}
	return false
}
